<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <ul>
            @foreach ($items as $item)
                <li>
                    <a href="{{ $item['url'] }}" target="_blank">
                        {{ $item['price'] }} - {{ $item['title'] }} ({{ $item['area'] }} m2) ({{$item['rooms']}} rooms) ({{ $item['beds'] }} beds)
                    </a>
                </li>
            @endforeach
        </ul>
    </body>
</html>
