<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {

    $url = "https://wunderflats.com/api/regions/12.880096436,52.650562346;14.043273926,52.160454558/query?minAccommodates=1&bbox=12.880096436,52.650562346%3B14.043273926,52.160454558&availableFrom=2018-09-01&page={page}";

    $page = 0;
    $itemsPerPage = 18;

    $allItems = [];

    while(true) {

        $browser = new Buzz\Browser();
        $response = $browser->get(str_replace('{page}', $page, $url));
        $data = json_decode($response->getContent(), 1);

        $items = $data['items'];
        $total = intval($data['total']);
        $totalPages = ceil($total / $itemsPerPage);

        $allItems = array_merge($allItems, $items);

        $page++;
        if($totalPages <= $page) {
            break;
        }
    }

    uasort($allItems, function($a, $b) {
        $priceA = floatval($a['price']);
        $priceB = floatval($b['price']);
        if ($priceA == $priceB) {
            return 0;
        }
        return $priceA < $priceB ? -1 : 1;
    });

    $allItems = array_map(function ($item) {
        $partOfGroup = !!$item['partOfGroup'];
        $id =  $partOfGroup ? $item['groupId'] : $item['_id'];
        $type = $partOfGroup ? 'g/' : '';
        return [
            'url' => sprintf("https://wunderflats.com/furnished-apartment/%sa/%s", $type, $id),
            'title' => $item['title']['en'],
            'price' => $item['price'] / 100,
            'area' => $item['area'],
            'beds' => $item['beds'],
            'rooms' => $item['rooms']
        ];
    }, $allItems);

    return view('fetch', ['items' => $allItems]);
});